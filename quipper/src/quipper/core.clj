(ns quipper.core
  (require [clojure.tools.cli :as cli])
  (require [clojure.string :as str])
  (require [clojure.java.io :as io])
  (require [clojure.edn :as edn])
  (:gen-class))

(def cli-options [["-h" "--help" "Print this help info"
                   :default false :flag true]
                  ["-f" "--file FILE" "Specify an alternate quip file."
                   :default (str
                             (System/getProperty "user.home")
                             "/.quips")]])
(defn show-banner
  []
  (println "This is the 'quipper' program."))

(defn file-exists?
  [file]
  (.exists (io/as-file file)))

(defn add-quip
  [file quips]
    (spit file (prn-str quips) :append true)
  quips)

(defn add-quips
  [file quips]
  (when (empty? quips) (show-banner))
  (add-quip file quips))

(defn drop-quips
  [file]
  (io/delete-file file true))

(defn quips-vector
  [file]
  (if (file-exists? file)
    (edn/read-string (slurp file))
    []))

(defn count-quips
  [file]
  (-> file
      quips-vector
      count
      println))

(defn get-quip
  [file]
  (let [quip (if-let [quips (-> file
                                  quips-vector
                                  seq)]
               (rand-nth quips)
               "")]
    (println quip)))

(defn process-action
  [file [action & quips]]
  (case action
    "add" (add-quips file quips)
    "drop" (drop-quips file)
    "count" (count-quips file)
    (get-quip file)))

(defn -main 
  "Deal with quips."
  [& args]
  (let [{:keys [options arguments summary errors]}
        (cli/parse-opts args cli-options)
        {:keys [file help]} options]
    (cond
     help (show-banner)
     :else (process-action file arguments))))
